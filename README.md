# Rust-lang on OSv

A simple example of building rust-lang project for OSv.

1. [Install Capstan](https://github.com/cloudius-systems/capstan/wiki/Capstan-Installation)
2. build image with `capstan build`
3. run your image with `capstan run`

$ capstan run
```
Created instance: osv-rust
OSv v0.24
eth0: 192.168.122.15
Hello, world!
```

tested with:
```
$ rustc --version
rustc 1.12.0 (3191fbae9 2016-09-23)
```
